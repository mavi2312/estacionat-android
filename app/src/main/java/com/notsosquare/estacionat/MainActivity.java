package com.notsosquare.estacionat;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.maps.android.SphericalUtil;
import com.notsosquare.estacionat.adapters.PlaceAutocompleteAdapter;
import com.notsosquare.estacionat.helpers.ParseClassesNames;
import com.notsosquare.estacionat.helpers.SlidingTabLayout;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    TabsPagerAdapter tabsPagerAdapter;
    ViewPager mViewPager;
    SlidingTabLayout mSlidingTabLayout;
    private GoogleApiClient mGoogleApiClient;
    MapFragment firstFragment;
    ListFragment secondFragment;
    MenuItem menuMap, menuList;

    public Location mLastLocation;

    private List<ParseObject> parkingList;

    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;

    private PlaceAutocompleteAdapter mAdapter;

    private ProgressDialog dialog;

    private AutoCompleteTextView mAutocompleteView;

    private ImageButton backSearchBar;
    private ImageButton clearSearchBar;

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();

    }

    //@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(getResources().getColor(R.color.primaryDarkApp));
        super.onCreate(savedInstanceState);
        //getWindow().setStatusBarColor(getResources().getColor(R.color.primaryDarkApp));
        setContentView(R.layout.activity_main);

        /*if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            // only for PreLollipop and newer versions

            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            // enable status bar tint
            tintManager.setStatusBarTintEnabled(true);
            // set a custom tint color for all system bars
            tintManager.setStatusBarTintResource(R.color.primaryDarkApp);
        }else{
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            // enable status bar tint
            tintManager.setStatusBarTintEnabled(false);
        }*/

        if (findViewById(R.id.fragmentContainer) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
//            firstFragment = new MapFragment();
//
//            // In case this activity was started with special instructions from an
//            // Intent, pass the Intent's extras to the fragment as arguments
//            Intent i = new Intent();
//            i.putExtra("someInt",0);
//            i.putExtra("someTitle","Mapa");
//
//            firstFragment.setArguments(i.getExtras());
//
//            secondFragment = new ListFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
//            Intent i2 = new Intent();
//            i2.putExtra("someInt",1);
//            i2.putExtra("someTitle","Lista");
//
//            secondFragment.setArguments(i2.getExtras());

            firstFragment = MapFragment.newInstance(0, "MAPA");
            secondFragment = ListFragment.newInstance(1,"LISTA");

            // Add the fragment to the 'fragment_container' FrameLayout
            //getSupportFragmentManager().beginTransaction()
             //       .add(R.id.fragmentContainer, firstFragment);
            //getSupportFragmentManager().beginTransaction()
              //      .add(R.id.fragmentContainer, secondFragment).commit();

            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fragmentContainer, firstFragment);
            transaction.add(R.id.fragmentContainer, secondFragment);
//            transaction.addToBackStack("MapToList");

            //transaction.show(firstFragment);
            transaction.hide(secondFragment);
            transaction.commit();

            MapsInitializer.initialize(this);
            if (savedInstanceState == null) {

// Prevent map from resetting when screen rotated
                firstFragment.setRetainInstance(true);

               //firtsFragment.setRetainInstance(true);

            }
        }
        // Initialize pager
//        tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
//        mViewPager = (ViewPager) findViewById(R.id.pager);
//        mViewPager.setAdapter(tabsPagerAdapter);

        // Initialize the tabs
//        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
//        mSlidingTabLayout.setSelectedIndicatorColors(Color.parseColor("#F88F1C"));
//        mSlidingTabLayout.setDividerColors(Color.parseColor("#F88F1C"));

        buildGoogleApiClient();

//        mSlidingTabLayout.setViewPager(mViewPager);



        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setTitle("Cargando");
        dialog.setMessage("Cargando datos");
        dialog.show();

        LatLngBounds chacaoBounds = convertCenterAndRadiusToBounds(new LatLng(10.499175,-66.854627),1700.0);
        //LatLngBounds chacaoBounds = convertCenterAndRadiusToBounds(new LatLng(51.484864,-0.098696),1600.0);



        backSearchBar = (ImageButton)findViewById(R.id.backButtonSearch);
        backSearchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert MainActivity.this.getSupportActionBar()!=null;
                MainActivity.this.getSupportActionBar().show();
                //MapFragment mapFragment = (MapFragment) tabsPagerAdapter.getRegisteredFragment(0);
                firstFragment.setParkingList(parkingList);
                //mainLayout.setPadding(0,getResources().getInteger(android.R.attr.actionBarSize),0,0);
            }
        });

        clearSearchBar = (ImageButton)findViewById(R.id.clearButtonSearch);
        clearSearchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
                mAutocompleteView.clearListSelection();
                mAutocompleteView.setText("");
                if (mLastLocation != null) {
                    ParseQuery<ParseObject> query = new ParseQuery<>("TestParkingLots");
                    query.whereNear(ParseClassesNames.LOCATION,new ParseGeoPoint(mLastLocation.getLatitude(),mLastLocation.getLongitude()));
                    query.setLimit(15);
                    query.whereGreaterThan(ParseClassesNames.CAPACITY,100);
                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> parkingListNew, ParseException e) {
                            dialog.dismiss();
                            if (e == null) {
                                parkingList = parkingListNew;
                                //getSupportFragmentManager().findFragmentByTag("mapa");

                                //MapFragment mapFragment = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);// tabsPagerAdapter.getRegisteredFragment(0);
                                firstFragment.setPlaceLocation(null);
                                firstFragment.setParkingList(parkingList);
//                                ListFragment listFragment = (ListFragment) tabsPagerAdapter.getRegisteredFragment(1);
                                secondFragment.setUserLocation(mLastLocation);
                                secondFragment.setParkingList(parkingList);
                                Log.e("MainActivity", "Retrieved " + parkingList.size() + " scores");

                            } else {
                                Log.e("MainActivity", "Error: " + e.getMessage());
                            }
                        }
                    });
                }else{
                    dialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                    // 2. Chain together various setter methods to set the dialog characteristics
                    builder.setMessage("No se ha podido detectar su posición actual. Revise que su GPS este encendido.")
                            .setTitle("Navegación").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        }
                    }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

                    // 3. Get the AlertDialog from create()
                    AlertDialog dialogGPS = builder.create();
                    dialogGPS.show();
                }
            }
        });

        // Retrieve the AutoCompleteTextView that will display Place suggestions.
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.searchBarPlaces);
        //AutocompleteView.setFocusableInTouchMode(true);

        // Register a listener that receives callbacks when a suggestion has been selected
        mAutocompleteView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a PlaceAutocomplete object from which we
             read the place ID.
              */
                final PlaceAutocompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);
                Log.i("Mapa", "Autocomplete item selected: " + item.description);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
              details about the place.
              */
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);


                //Toast.makeText(getApplicationContext(), "Clicked: " + item.description, Toast.LENGTH_SHORT).show();
                Log.i("Mapa", "Called getPlaceById to get Place details for " + item.placeId);
            }
        });


        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, chacaoBounds, null);
        mAutocompleteView.setAdapter(mAdapter);



    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    /*public GoogleApiClient getmGoogleApiClient(){
        return mGoogleApiClient;
    }*/



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menuMap = menu.findItem(R.id.action_map);
        menuList = menu.findItem(R.id.action_list) ;
        menuMap.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_list) {
            //assert MainActivity.this.getSupportActionBar()!=null;
            //MainActivity.this.getSupportActionBar().hide();
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

//            transaction.addToBackStack("MapToList");

            transaction.hide(firstFragment);
            transaction.show(secondFragment);
            transaction.commit();
            secondFragment.setUserLocation(mLastLocation);
            //secondFragment.setParkingList(parkingList);
            item.setVisible(false);
            menuMap.setVisible(true);
            menuList.setVisible(false);
            return true;
        }else if(id == R.id.action_map){
            android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.hide(secondFragment);
            transaction.show(firstFragment);
            //transaction.add(R.id.fragmentContainer, firstFragment, "Map");
//            transaction.addToBackStack("ListToMap");
            menuMap.setVisible(false);
            menuList.setVisible(true);
            transaction.commit();
            secondFragment.setUserLocation(mLastLocation);
            //secondFragment.setParkingList(parkingList);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

//        if(getSupportFragmentManager().findFragmentByTag("List") != null){
//            getSupportFragmentManager().popBackStack("MapToList", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        }else if(getSupportFragmentManager().findFragmentByTag("Map") != null){
//            getSupportFragmentManager().popBackStack("ListToMap", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        }else{
            super.onBackPressed();
//        }
    }

    public List<ParseObject> getParkingData(){
        return parkingList;
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            ParseQuery<ParseObject> query = new ParseQuery<>("TestParkingLots");
            query.whereNear(ParseClassesNames.LOCATION,new ParseGeoPoint(mLastLocation.getLatitude(),mLastLocation.getLongitude()));
            query.setLimit(15);
            query.whereGreaterThan(ParseClassesNames.CAPACITY,100);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> parkingListNew, ParseException e) {
                    dialog.dismiss();
                    if (e == null) {
                        parkingList = parkingListNew;
                        //getSupportFragmentManager().findFragmentByTag("mapa");
                        //MapFragment mapFragment = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);// tabsPagerAdapter.getRegisteredFragment(0);
                        firstFragment.setParkingList(parkingList);
//                        ListFragment listFragment = (ListFragment) tabsPagerAdapter.getRegisteredFragment(1);
                        secondFragment.setUserLocation(mLastLocation);
                        secondFragment.setParkingList(parkingList);

                        Log.e("MainActivity", "Retrieved " + parkingList.size() + " scores");

                    } else {
                        Log.e("MainActivity", "Error: " + e.getMessage());
                    }
                }
            });
        }else{
            dialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage("No se ha podido detectar su posición actual. Revise que su GPS este encendido.")
                    .setTitle("Navegación").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(callGPSSettingIntent);
                }
            }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialogGPS = builder.create();
            dialogGPS.show();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingError) {
            // Already attempting to resolve an error.
            Log.e("onConectionFailed","ya se esta resolviendo el error");
        } else if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(connectionResult.getErrorCode());
            mResolvingError = true;
        }

    }

    // The rest of this code is all about building the error dialog

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((MainActivity)getActivity()).onDialogDismissed();
        }
    }

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e("MapFragment", "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }

            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            // Get the Place object from the buffer.
            final Place selected = places.get(0);

            mAutocompleteView.setText(selected.getName(),false);

            dialog.show();

            //MapFragment mapFragment = (MapFragment) tabsPagerAdapter.getRegisteredFragment(0);
            firstFragment.setPlaceLocation(selected);
            ParseQuery<ParseObject> query = new ParseQuery<>("TestParkingLots");
            query.whereNear(ParseClassesNames.LOCATION,new ParseGeoPoint(selected.getLatLng().latitude,selected.getLatLng().longitude));
            query.whereGreaterThan(ParseClassesNames.CAPACITY,100);
            query.setLimit(15);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> parkingListNew, ParseException e) {
                    dialog.dismiss();
                    if (e == null) {
                        parkingList = parkingListNew;
                        //getSupportFragmentManager().findFragmentByTag("mapa");
                        //MapFragment mapFragment = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);// tabsPagerAdapter.getRegisteredFragment(0);
                        firstFragment.setParkingList(parkingList);
//                        ListFragment listFragment = (ListFragment) tabsPagerAdapter.getRegisteredFragment(1);
//                        listFragment.setUserLocation(mLastLocation);
//                        listFragment.setParkingList(parkingList);
//                        Log.e("MainActivity", "Retrieved " + parkingList.size() + " scores");
                        secondFragment.setUserLocation(mLastLocation);
                        secondFragment.setParkingList(parkingList);

                    } else {
                        Log.e("MainActivity", "Error: " + e.getMessage());
                    }
                }
            });


            places.release();
        }
    };

    public LatLngBounds convertCenterAndRadiusToBounds(LatLng center, double radius){
        LatLng southwest = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0), 225);
        LatLng northeast = SphericalUtil.computeOffset(center, radius * Math.sqrt(2.0),45);
        return new LatLngBounds(southwest,northeast);
    }


}
