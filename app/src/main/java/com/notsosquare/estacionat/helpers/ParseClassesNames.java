package com.notsosquare.estacionat.helpers;

/**
 * Created by MaríaVirginia on 28-06-2015.
 */
public interface ParseClassesNames {

    String NAME = "name";
    String ALIAS = "alias";
    String CAPACITY = "capacity";
    String LOCATION = "location";
    String AVAILABLE = "available";
    String PRICE = "price";
    String SHORT_ADDRESS = "shortAddress";
    String LONG_ADDRESS = "longAddress";
    String RIF = "rif";
    String NUMBER = "number";
    String PHONE = "phone";
    String PUBLIC = "public";
    String TICKET_CARD = "ticketCard";
    String RENTAL_SPACES = "rentalSpaces";
    String OWNED_SPACES = "ownedSpaces";
    String BIKE_SPACES = "bikeSpaces";
    String OVERNIGHT = "overnight";
    String WEEK = "week";
    String WEEKEND = "weekend";
    String IMAGE = "image";
    String OPEN = "open";
    String NOTE = "note";
    String TYPE = "type";
    String MONDAY = "monday";
    String TUESDAY = "tuesday";
    String WEDNESDAY = "wednesday";
    String THURSDAY = "thursday";
    String FRIDAY = "friday";
    String SATURDAY = "saturday";
    String SUNDAY = "sunday";

    
}
