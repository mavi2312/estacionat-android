package com.notsosquare.estacionat.helpers;

import android.location.Location;

import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.DataFormatException;

/**
 * Created by MaríaVirginia on 27-06-2015.
 */
public class OrderParkingLots {


    public OrderParkingLots(){

    }

    public ArrayList<ParseObject> orderListAvailability(ArrayList<ParseObject> list){

        ParseObject temp;

        for(int i=0; i < list.size()-1; i++){

            for(int j=1; j < list.size()-i; j++){
                //float rate1 = getRate(list.get(j-1));
                //float rate2 = getRate(list.get(j));
                int rate1 = list.get(j-1).getInt(ParseClassesNames.CAPACITY);
                int rate2 = list.get(j).getInt(ParseClassesNames.CAPACITY);

                if(rate1  < rate2){
                    temp=list.get(j-1);

                    list.set(j-1,list.get(j));
                    list.set(j,temp);

                }

            }

        }
        return list;
    }

    public ArrayList<ParseObject> orderListPrice(ArrayList<ParseObject> list){

        ParseObject temp;

        for(int i=0; i < list.size()-1; i++){

            for(int j=1; j < list.size()-i; j++){
                int price1 = list.get(j-1).getInt(ParseClassesNames.PRICE);
                int price2 = list.get(j).getInt(ParseClassesNames.PRICE);

                if(price1  > price2){
                    temp=list.get(j-1);
                    list.set(j-1,list.get(j));
                    list.set(j,temp);

                }

            }

        }
        return list;
    }

    public ArrayList<ParseObject> orderListLocation(ArrayList<ParseObject> list, Location user){

        ParseObject temp;

        for(int i=0; i < list.size()-1; i++){

            for(int j=1; j < list.size()-i; j++){
                double distance1 = getDistance(user,list.get(j-1));
                double distance2 = getDistance(user, list.get(j));

                if(distance1  > distance2){
                    temp=list.get(j-1);
                    list.set(j-1,list.get(j));
                    list.set(j,temp);

                }

            }

        }
        return list;
    }

    public float getRate(ParseObject firstElement){
        if(firstElement.has(ParseClassesNames.CAPACITY) && firstElement.has(ParseClassesNames.AVAILABLE))
            return (float)(firstElement.getInt(ParseClassesNames.CAPACITY)-firstElement.getInt(ParseClassesNames.AVAILABLE))/(float)firstElement.getInt(ParseClassesNames.CAPACITY);
        else
            return (float)0.0;
    }

    public double getDistance(Location user, ParseObject parkingLot){
        ParseGeoPoint parkingLocation = parkingLot.getParseGeoPoint(ParseClassesNames.LOCATION);

        return parkingLocation.distanceInKilometersTo(new ParseGeoPoint(user.getLatitude(),user.getLongitude()));

    }

    public boolean isOpen(ParseObject parkingLot){
        Calendar c = Calendar.getInstance();
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        int day = c.get(Calendar.DAY_OF_WEEK);

        boolean condition = false;

        switch (day){
            case Calendar.MONDAY:
                if (parkingLot.has(ParseClassesNames.MONDAY)) {
                    HashMap schedule = (HashMap) parkingLot.get(ParseClassesNames.MONDAY);
                    if (schedule.containsKey("open")) {
                        if ((boolean) schedule.get("open")) {
                            if (schedule.containsKey("schedule")) {
                                if(schedule.get("schedule").equals("24h"))
                                    condition = true;
                                else {
                                    String hours = (String) schedule.get("schedule");
                                    String[] separatedHours = hours.split(" ");
                                    SimpleDateFormat format = new SimpleDateFormat("hh:mma", Locale.getDefault());

                                    try {
                                        Date openingTime = format.parse(separatedHours[0]);
                                        Date closingTime = format.parse(separatedHours[2]);
                                        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), openingTime.getHours(), openingTime.getMinutes(), 0);
                                        if(separatedHours[2].contains("am")){
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)+1, closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }else {
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }
                                        condition = (c.after(c1) && c.before(c2));

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        condition = false;
                                    }
                                }
                            } else {
                                condition = false;
                            }
                        } else {
                            condition = false;
                        }
                    } else
                        condition = false;
                }else
                    condition = false;
                break;

            case Calendar.TUESDAY:
                if (parkingLot.has(ParseClassesNames.TUESDAY)) {
                    HashMap schedule2 = (HashMap) parkingLot.get(ParseClassesNames.TUESDAY);
                    if (schedule2.containsKey("open")) {
                        if ((boolean) schedule2.get("open")) {
                            if (schedule2.containsKey("schedule")) {
                                if(schedule2.get("schedule").equals("24h"))
                                    condition = true;
                                else {
                                    String hours = (String) schedule2.get("schedule");
                                    String[] separatedHours = hours.split(" ");
                                    SimpleDateFormat format = new SimpleDateFormat("hh:mma", Locale.getDefault());

                                    try {
                                        Date openingTime = format.parse(separatedHours[0]);
                                        Date closingTime = format.parse(separatedHours[2]);
                                        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), openingTime.getHours(), openingTime.getMinutes(), 0);
                                        if(separatedHours[2].contains("am")){
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)+1, closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }else {
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }
                                        condition = (c.after(c1) && c.before(c2));

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        condition = false;
                                    }
                                }
                            } else {
                                condition = false;
                            }
                        } else {
                            condition = false;
                        }
                    } else
                        condition = false;
                }else
                    condition = false;
                break;
            case Calendar.WEDNESDAY:
                if (parkingLot.has(ParseClassesNames.WEDNESDAY)) {
                    HashMap schedule3 = (HashMap) parkingLot.get(ParseClassesNames.WEDNESDAY);
                    if (schedule3.containsKey("open")) {
                        if ((boolean) schedule3.get("open")) {
                            if (schedule3.containsKey("schedule")) {
                                if(schedule3.get("schedule").equals("24h"))
                                    condition = true;
                                else {
                                    String hours = (String) schedule3.get("schedule");
                                    String[] separatedHours = hours.split(" ");
                                    SimpleDateFormat format = new SimpleDateFormat("hh:mma", Locale.getDefault());

                                    try {
                                        Date openingTime = format.parse(separatedHours[0]);
                                        Date closingTime = format.parse(separatedHours[2]);
                                        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), openingTime.getHours(), openingTime.getMinutes(), 0);
                                        if(separatedHours[2].contains("am")){
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)+1, closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }else {
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }
                                        condition = (c.after(c1) && c.before(c2));

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        condition = false;
                                    }
                                }
                            } else {
                                condition = false;
                            }
                        } else {
                            condition = false;
                        }
                    } else
                        condition = false;
                }else
                    condition= false;
                break;
            case Calendar.THURSDAY:
                if(parkingLot.has(ParseClassesNames.THURSDAY)) {
                    HashMap schedule4 = (HashMap) parkingLot.get(ParseClassesNames.THURSDAY);
                    if (schedule4.containsKey("open")) {
                        if ((boolean) schedule4.get("open")) {
                            if (schedule4.containsKey("schedule")) {
                                if(schedule4.get("schedule").equals("24h"))
                                    condition = true;
                                else {
                                    String hours = (String) schedule4.get("schedule");
                                    String[] separatedHours = hours.split(" ");
                                    SimpleDateFormat format = new SimpleDateFormat("hh:mma", Locale.getDefault());

                                    try {
                                        Date openingTime = format.parse(separatedHours[0]);
                                        Date closingTime = format.parse(separatedHours[2]);
                                        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), openingTime.getHours(), openingTime.getMinutes(), 0);
                                        if(separatedHours[2].contains("am")){
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)+1, closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }else {
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }
                                        condition = (c.after(c1) && c.before(c2));

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        condition = false;
                                    }
                                }
                            } else {
                                condition = false;
                            }
                        } else {
                            condition = false;
                        }
                    } else
                        condition = false;
                }else
                    condition = false;
                break;
            case Calendar.FRIDAY:
                if(parkingLot.has(ParseClassesNames.FRIDAY)) {
                    HashMap schedule5 = (HashMap) parkingLot.get(ParseClassesNames.FRIDAY);
                    if (schedule5.containsKey("open")) {
                        if ((boolean) schedule5.get("open")) {
                            if (schedule5.containsKey("schedule")) {
                                if(schedule5.get("schedule").equals("24h"))
                                    condition = true;
                                else {
                                    String hours = (String) schedule5.get("schedule");
                                    String[] separatedHours = hours.split(" ");
                                    SimpleDateFormat format = new SimpleDateFormat("hh:mma", Locale.getDefault());

                                    try {
                                        Date openingTime = format.parse(separatedHours[0]);
                                        Date closingTime = format.parse(separatedHours[2]);
                                        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), openingTime.getHours(), openingTime.getMinutes(), 0);
                                        if(separatedHours[2].contains("am")){
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)+1, closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }else {
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }
                                        condition = (c.after(c1) && c.before(c2));

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        condition = false;
                                    }
                                }
                            } else {
                                condition = false;
                            }
                        } else {
                            condition = false;
                        }
                    } else
                        condition = false;
                }else
                    condition = false;
                break;

            case Calendar.SATURDAY:
                if(parkingLot.has(ParseClassesNames.SATURDAY)) {
                    HashMap schedule6 = (HashMap) parkingLot.get(ParseClassesNames.SATURDAY);
                    if (schedule6.containsKey("open")) {
                        if ((boolean) schedule6.get("open")) {
                            if (schedule6.containsKey("schedule")) {
                                if(schedule6.get("schedule").equals("24h"))
                                    condition = true;
                                else {
                                    String hours = (String) schedule6.get("schedule");
                                    String[] separatedHours = hours.split(" ");
                                    SimpleDateFormat format = new SimpleDateFormat("hh:mma", Locale.getDefault());

                                    try {
                                        Date openingTime = format.parse(separatedHours[0]);
                                        Date closingTime = format.parse(separatedHours[2]);
                                        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), openingTime.getHours(), openingTime.getMinutes(), 0);
                                        if(separatedHours[2].contains("am")){
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)+1, closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }else {
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }
                                        condition = (c.after(c1) && c.before(c2));

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        condition = false;
                                    }
                                }

                            } else {
                                condition = false;
                            }
                        } else {
                            condition = false;
                        }
                    } else
                        condition = false;
                }else
                    condition = false;
                break;
            case Calendar.SUNDAY:
                if(parkingLot.has(ParseClassesNames.SUNDAY)) {
                    HashMap schedule7 = (HashMap) parkingLot.get(ParseClassesNames.SUNDAY);

                    if (schedule7.containsKey("open")) {
                        if ((boolean) schedule7.get("open")) {
                            if (schedule7.containsKey("schedule")) {
                                if(schedule7.get("schedule").equals("24h"))
                                    condition = true;
                                else {
                                    String hours = (String) schedule7.get("schedule");
                                    String[] separatedHours = hours.split(" ");
                                    SimpleDateFormat format = new SimpleDateFormat("hh:mma", Locale.getDefault());

                                    try {
                                        Date openingTime = format.parse(separatedHours[0]);
                                        Date closingTime = format.parse(separatedHours[2]);
                                        c1.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), openingTime.getHours(), openingTime.getMinutes(), 0);
                                        if(separatedHours[2].contains("am")){
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)+1, closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }else {
                                            c2.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), closingTime.getHours(), closingTime.getMinutes(), 0);
                                        }
                                        condition = (c.after(c1) && c.before(c2));

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        condition = false;
                                    }
                                }
                            } else {
                                condition = false;
                            }
                        } else {
                            condition = false;
                        }
                    } else
                        condition = false;
                }else
                    condition = false;
                break;
        }

        return condition;
    }

}
