package com.notsosquare.estacionat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.notsosquare.estacionat.adapters.ParkingListAdapter;
import com.notsosquare.estacionat.adapters.PlaceAutocompleteAdapter;
import com.notsosquare.estacionat.helpers.HttpConnection;
import com.notsosquare.estacionat.helpers.OrderParkingLots;
import com.notsosquare.estacionat.helpers.ParseClassesNames;
import com.notsosquare.estacionat.helpers.PathJSONParser;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Roberto on 6/21/2015.
 */
public class MapFragment extends Fragment {

    private String title;
    private int page;
    private MapView mapView;
    private GoogleMap map;

    private List<ParseObject> parkingList;

    /*Variables para el botón de filtros*/

    RelativeLayout filterBackground;

    OrderParkingLots orderHelper;

    FloatingActionButton filters;
    FloatingActionButton navigation;

    FloatingActionButton actionLocation;
    FloatingActionButton actionFilters;
    FloatingActionButton actionSearch;
    LinearLayout bottomBar;

    ParseObject parkingNav;
    Marker markerNav;
    Marker selectedMarker;

    private Place selected;
    private CharSequence[] parkingNames;

    private ProgressDialog dialogNav;

    public MapFragment() {

    }

    // newInstance constructor for creating fragment with arguments
    public static MapFragment newInstance(int page, String title) {
        MapFragment mapFragment = new MapFragment();

        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        mapFragment.setArguments(args);
        return mapFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setRetainInstance(true);
    }

    //     Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment, container, false);
        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) view.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);


        // Gets to GoogleMap from the MapView and does initialization stuff
        map = mapView.getMap();
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMapToolbarEnabled(false);

        orderHelper = new OrderParkingLots();
        dialogNav = new ProgressDialog(getActivity());
        dialogNav.setIndeterminate(true);
        dialogNav.setTitle("Cargando");
        dialogNav.setMessage("Calculando la ruta más rápida al estacionamiento seleccionado.");

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls

        //MapsInitializer.initialize(this.getActivity());

        MainActivity main = (MainActivity)getActivity();

        if(parkingList!=null){
            filterParking(parkingList, "capacity");
        }

        bottomBar = (LinearLayout)view.findViewById(R.id.bottomBar);
        bottomBar.setVisibility(View.GONE);


        LatLngBounds chacaoBounds = main.convertCenterAndRadiusToBounds(new LatLng(10.499175,-66.854627),1700.0);

        //map.animateCamera(CameraUpdateFactory.newLatLngBounds(chacaoBounds, 0));


        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(chacaoBounds.getCenter(),14);
        map.moveCamera(cameraUpdate);

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                bottomBar.setVisibility(View.GONE);
                filters.setVisibility(View.VISIBLE);
                navigation.setVisibility(View.GONE);
            }
        });

        filters = (FloatingActionButton) view.findViewById(R.id.fabMap);
        navigation = (FloatingActionButton) view.findViewById(R.id.fabMapNav);
        filterBackground = (RelativeLayout)view.findViewById(R.id.filterBackgroundMap);

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 17));
                bottomBar.setVisibility(View.VISIBLE);
                TextView alias = (TextView)bottomBar.findViewById(R.id.parkingMapAlias);
                TextView filterText = (TextView)bottomBar.findViewById(R.id.parkingMapFilter);
                alias.setText(marker.getTitle());
                filterText.setText(marker.getSnippet());
                filters.setVisibility(View.GONE);
                navigation.setVisibility(View.VISIBLE);
                selectedMarker = marker;
                return true;
            }
        });

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Getting view from the layout file info_window_layout
                View v = getActivity().getLayoutInflater().inflate(R.layout.custom_infowindow, null);

                // Getting the position from the marker
                LatLng latLng = marker.getPosition();

                // Getting reference to the TextView to set latitude
                TextView tvLat = (TextView) v.findViewById(R.id.markerName);

                // Getting reference to the TextView to set longitude
                TextView tvLng = (TextView) v.findViewById(R.id.markerFilter);

                // Setting the latitude
                tvLat.setText(marker.getTitle());

                // Setting the longitude
                tvLng.setText(marker.getSnippet());

                // Returning the view containing InfoWindow contents
                return v;
            }
        });

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(final Marker marker) {
                String url="";
                if(selected!=null){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Seleccione un estacionamiento cercano")
                            .setItems(parkingNames, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    for(int i=0;i<parkingNames.length;i++){
                                        Log.e("parkingNames", String.valueOf(parkingNames[i]));
                                        Log.e("parkingList", parkingList.get(i).getString(ParseClassesNames.ALIAS));
                                    }
                                    setNavigation(selected,parkingList.get(which));
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }else {
                    if(marker.getSnippet().equals("Cerrado")){
                        // 1. Instantiate an AlertDialog.Builder with its constructor
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        // 2. Chain together various setter methods to set the dialog characteristics
                        builder.setMessage("Este estacionamiento esta cerrado.")
                                .setTitle("Navegación").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        // 3. Get the AlertDialog from create()
                        AlertDialog dialog = builder.create();

                        dialog.show();
                    }else{
                        setNavigationParking(marker);
                    }
                }


            }
        });



        filters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filterBackground.getVisibility()==View.GONE)
                    filterBackground.setVisibility(View.VISIBLE);
                else
                    filterBackground.setVisibility(View.GONE);
            }
        });

        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url="";
                if(selected!=null){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Seleccione un estacionamiento cercano")
                            .setItems(parkingNames, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    for(int i=0;i<parkingNames.length;i++){
                                        Log.e("parkingNames", String.valueOf(parkingNames[i]));
                                        Log.e("parkingList", parkingList.get(i).getString(ParseClassesNames.ALIAS));
                                    }
                                    setNavigation(selected,parkingList.get(which));
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }else {
                    if(selectedMarker.getSnippet().equals("Cerrado")){
                        // 1. Instantiate an AlertDialog.Builder with its constructor
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        // 2. Chain together various setter methods to set the dialog characteristics
                        builder.setMessage("Este estacionamiento esta cerrado.")
                                .setTitle("Navegación").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        // 3. Get the AlertDialog from create()
                        AlertDialog dialog = builder.create();

                        dialog.show();
                    }else{
                        setNavigationParking(selectedMarker);
                    }
                }
/*
                navigation.setVisibility(View.GONE);
                filters.setVisibility(View.VISIBLE);
                String lat="",lng="";
                if(parkingNav!=null){
                    lat = String.valueOf(parkingNav.getParseGeoPoint(ParseClassesNames.LOCATION).getLatitude());
                    lng = String.valueOf(parkingNav.getParseGeoPoint(ParseClassesNames.LOCATION).getLongitude());
                }else if(markerNav!=null){
                    lat = String.valueOf(markerNav.getPosition().latitude);
                    lng = String.valueOf(markerNav.getPosition().longitude);
                }
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+lat+","+lng);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);*/

            }
        });

        actionLocation = (FloatingActionButton) view.findViewById(R.id.fabMiniMap1);
        actionLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*filtersMiniFavorites.setBackgroundTintList(getResources().getColorStateList(R.color.selected_filter));
                filtersMiniPrice.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniAvailability.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));*/
                //filterParking(parkingList, "");
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude()), 14);
                map.animateCamera(cameraUpdate);
                filterBackground.setVisibility(View.GONE);

            }
        });

        actionFilters = (FloatingActionButton) view.findViewById(R.id.fabMiniMap2);
        actionFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*filtersMiniPrice.setBackgroundTintList(getResources().getColorStateList(R.color.selected_filter));
                filtersMiniFavorites.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniAvailability.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filterParking(parkingList, "price");*/

                filterBackground.setVisibility(View.GONE);
            }
        });

        actionSearch = (FloatingActionButton) view.findViewById(R.id.fabMiniMap3);
        actionSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*filtersMiniAvailability.setBackgroundTintList(getResources().getColorStateList(R.color.selected_filter));
                filtersMiniFavorites.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniPrice.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));

                filterParking(parkingList, "capacity");*/

                ((MainActivity) getActivity()).getSupportActionBar().hide();
                filterBackground.setVisibility(View.GONE);

            }
        });

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {

            final float scale = getActivity().getResources().getDisplayMetrics().density;
            int pixels = (int) (10 * scale + 0.5f);
            int pixelsRight = (int) (23 * scale + 0.5f);

            RelativeLayout.LayoutParams p2 = (RelativeLayout.LayoutParams)actionFilters.getLayoutParams();
            p2.setMargins(0,0,pixelsRight,pixels);
            actionFilters.setLayoutParams(p2);
            RelativeLayout.LayoutParams p3 = (RelativeLayout.LayoutParams)actionSearch.getLayoutParams();
            p3.setMargins(0,0,pixelsRight,pixels);
            actionSearch.setLayoutParams(p3);
            RelativeLayout.LayoutParams p4 = (RelativeLayout.LayoutParams)actionLocation.getLayoutParams();
            p4.setMargins(0,0,pixelsRight,pixels);
            actionLocation.setLayoutParams(p4);
        }



        return view;
    }

    public void setPlaceLocation(Place selected){
        if(selected!=null) {
            this.selected = selected.freeze();
            map.addMarker((new MarkerOptions().position(selected.getLatLng())).title(selected.getName().toString()).snippet(selected.getAddress().toString()));
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(selected.getLatLng(), 15);
            map.animateCamera(cameraUpdate);
        }else{
            this.selected = null;
            MainActivity main = (MainActivity)getActivity();

            LatLngBounds chacaoBounds = main.convertCenterAndRadiusToBounds(new LatLng(10.499175,-66.854627),1700.0);

            //map.animateCamera(CameraUpdateFactory.newLatLngBounds(chacaoBounds, 0));


            // Updates the location and zoom of the MapView
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(chacaoBounds.getCenter(),14);
            map.animateCamera(cameraUpdate);
        }
    }


    public void setNavigation(Place destination, ParseObject wayPoint){
        map.clear();
        LatLng positionWayPoint = new LatLng(wayPoint.getParseGeoPoint(ParseClassesNames.LOCATION).getLatitude(),wayPoint.getParseGeoPoint(ParseClassesNames.LOCATION).getLongitude());
        map.addMarker((new MarkerOptions().position(selected.getLatLng())).title(selected.getName().toString()).snippet(selected.getAddress().toString()));
        map.addMarker((new MarkerOptions().position(positionWayPoint)).title(wayPoint.getString(ParseClassesNames.ALIAS)).snippet(wayPoint.getString(ParseClassesNames.SHORT_ADDRESS))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.orange_pin));
        String url = getMapsApiDirectionsUrl(new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude()), positionWayPoint,null);
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);
        parkingNav = wayPoint;
    }

    public void setNavigationParking(Marker parkingLot){
        map.clear();
        map.addMarker((new MarkerOptions().position(parkingLot.getPosition())).title(parkingLot.getTitle()).snippet(parkingLot.getSnippet())).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.orange_pin));
        String url = getMapsApiDirectionsUrl(new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude()), parkingLot.getPosition(),null);
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);
        markerNav = parkingLot;
    }

    public void setParkingList(List<ParseObject> newList){
        //parkingList = orderHelper.orderListLocation((ArrayList<ParseObject>) newList,map.getMyLocation());
        parkingList = newList;
        filterParking(parkingList,"capacity");
        List<String> listItems = new ArrayList<String>();
        for(int i=0;i<parkingList.size();i++){
            listItems.add(i,parkingList.get(i).getString(ParseClassesNames.ALIAS));
        }
        parkingNames = listItems.toArray(new CharSequence[listItems.size()]);
        for(int i=0;i<parkingNames.length;i++){
            Log.e("parkingNames1", String.valueOf(parkingNames[i]));
            Log.e("parkingList1", parkingList.get(i).getString(ParseClassesNames.ALIAS));
        }
        navigation.setVisibility(View.GONE);
        filters.setVisibility(View.VISIBLE);
        parkingNav=null;
        markerNav=null;
    }

    public void filterParking(List<ParseObject> parkingList, String type){
        map.clear();
//        map.addCircle(new CircleOptions().center(new LatLng(10.499334,-66.854712)).radius(1700.0).strokeColor(Color.RED));
//        map.addMarker((new MarkerOptions().position(new LatLng(10.499334,-66.854712))
//                .title("centro").snippet("Cerrado")));
        if(selected!=null)
            map.addMarker((new MarkerOptions().position(selected.getLatLng())).title(selected.getName().toString()).snippet(selected.getAddress().toString()));
        for(int i=0;i<parkingList.size();i++){
            ParseGeoPoint parkingLocation = parkingList.get(i).getParseGeoPoint("location");
            if (parkingLocation!=null){
                if(!orderHelper.isOpen(parkingList.get(i))){
                    map.addMarker((new MarkerOptions().position(new LatLng(parkingLocation.getLatitude(), parkingLocation.getLongitude()))
                            .title(parkingList.get(i).getString(ParseClassesNames.ALIAS)).snippet("Cerrado"))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.gray_pin));
                }else if(type.contentEquals(ParseClassesNames.CAPACITY)){
                    Log.e("MapFilter","Rate de disponibles: "+String.valueOf(orderHelper.getRate(parkingList.get(i))));
                    //if(orderHelper.getRate(parkingList.get(i))>(float)0.9) {
                    if(parkingList.get(i).getInt(ParseClassesNames.CAPACITY)>500) {
                        map.addMarker((new MarkerOptions().position(new LatLng(parkingLocation.getLatitude(), parkingLocation.getLongitude()))
                                .title(parkingList.get(i).getString(ParseClassesNames.ALIAS)).snippet("Capacidad: "+parkingList.get(i).getInt(ParseClassesNames.CAPACITY)))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.green_pin));
                    }
                    //else if(orderHelper.getRate(parkingList.get(i))> (float)0.5){
                    else if(parkingList.get(i).getInt(ParseClassesNames.CAPACITY)>200) {
                        map.addMarker((new MarkerOptions().position(new LatLng(parkingLocation.getLatitude(), parkingLocation.getLongitude()))
                                .title(parkingList.get(i).getString(ParseClassesNames.ALIAS)).snippet("Capacidad: "+parkingList.get(i).getInt(ParseClassesNames.CAPACITY)))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_pin));
                    }else{
                        map.addMarker((new MarkerOptions().position(new LatLng(parkingLocation.getLatitude(), parkingLocation.getLongitude()))
                                .title(parkingList.get(i).getString(ParseClassesNames.ALIAS)).snippet("Capacidad: " + parkingList.get(i).getInt(ParseClassesNames.CAPACITY)))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_pin));
                    }
                }else if (type.contentEquals(ParseClassesNames.PRICE)){
                    if(parkingList.get(i).getInt(type)>13) {
                        map.addMarker((new MarkerOptions().position(new LatLng(parkingLocation.getLatitude(), parkingLocation.getLongitude()))
                                .title(parkingList.get(i).getString(ParseClassesNames.ALIAS)).snippet("Precio: " + parkingList.get(i).getInt(ParseClassesNames.PRICE)))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.red_pin));
                    }else if(parkingList.get(i).getInt(type)>9
                            ){
                        map.addMarker((new MarkerOptions().position(new LatLng(parkingLocation.getLatitude(), parkingLocation.getLongitude()))
                                .title(parkingList.get(i).getString(ParseClassesNames.ALIAS)).snippet("Precio: " + parkingList.get(i).getInt(ParseClassesNames.PRICE)))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_pin));
                    }else{
                        map.addMarker((new MarkerOptions().position(new LatLng(parkingLocation.getLatitude(), parkingLocation.getLongitude()))
                                .title(parkingList.get(i).getString(ParseClassesNames.ALIAS)).snippet("Precio: " + parkingList.get(i).getInt(ParseClassesNames.PRICE)))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.green_pin));
                    }
                }else{
                    map.addMarker((new MarkerOptions().position(new LatLng(parkingLocation.getLatitude(), parkingLocation.getLongitude()))
                            .title(parkingList.get(i).getString(ParseClassesNames.ALIAS)).snippet("Favorito"))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.fav_pin));
                }
            }
        }
    }

    private String getMapsApiDirectionsUrl(LatLng origin, LatLng destination, LatLng wayPoint) {
        String waypoints="";
        if(wayPoint!=null) {
            waypoints = "waypoints=optimize:true|"
                    + wayPoint.latitude + ","
                    + wayPoint.longitude;
        }
        String originString = "origin="+String.valueOf(origin.latitude) + "," + String.valueOf(origin.longitude);
        String destinationString = "destination="+String.valueOf(destination.latitude) + "," + String.valueOf(destination.longitude);


        String sensor = "sensor=false";
        String params = originString + "&" + destinationString + "&" + sensor + "key=AIzaSyCBxqx-SS3AjIg-49H_pqx6v0QqeYcXGUU";
        if(!waypoints.equals("")){
            params+= "&" +waypoints;
        }
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + params;
        Log.e("urlNav",url);
        return url;
    }

    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogNav.show();
        }

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("NavResult",result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            dialogNav.dismiss();
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(5);
                polyLineOptions.color(Color.BLUE);
            }

            map.addPolyline(polyLineOptions);

            filters.setVisibility(View.GONE);
            navigation.setVisibility(View.VISIBLE);

        }
    }


    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }




}
