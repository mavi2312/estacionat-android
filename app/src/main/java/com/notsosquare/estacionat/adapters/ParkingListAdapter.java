package com.notsosquare.estacionat.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.notsosquare.estacionat.R;
import com.notsosquare.estacionat.helpers.ParseClassesNames;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by MaríaVirginia on 22-06-2015.
 */
public class ParkingListAdapter extends BaseAdapter {

    private List<ParseObject> parkingList;
    private LayoutInflater inflater = null;
    private static final String TAG = "ParkingListAdapter";
    private int convertViewCounter;
    private Context c;

    private static final int TYPE_FIRST_ITEM = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_MAX_COUNT = TYPE_ITEM + 1;

    static class ViewHolder
    {
        TextView tvParkingName;
        TextView tvParkingAvailability;
        TextView tvParkingPrice;
        ImageView ivParkingImage;
        View vParkingStatus;

    }

    public ParkingListAdapter(Context c, List<ParseObject> list){
        this.parkingList = list;
        inflater = LayoutInflater.from(c);
        this.c=c;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? TYPE_FIRST_ITEM : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    @Override
    public int getCount() {
        return parkingList.size();
    }

    @Override
    public Object getItem(int position) {
        return parkingList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder holder;

        Log.v(TAG, "in getView for position " + position + ", convertView is "
                + ((convertView == null) ? "null" : "being recycled"));

        int type = getItemViewType(position);

        if (convertView == null)
        {
            //Log.e(TAG,"Position: "+String.valueOf(position));
            convertViewCounter++;
            Log.v(TAG, convertViewCounter + " convertViews have been created");
            holder = new ViewHolder();
           switch(type){
               case 0:
                convertView = inflater.inflate(R.layout.park_list_header_item, null);

                //convertViewCounter++;
                //Log.v(TAG, convertViewCounter + " convertViews have been created");

                holder.tvParkingName = (TextView) convertView
                        .findViewById(R.id.firstParkingName);
                holder.tvParkingAvailability = (TextView) convertView
                        .findViewById(R.id.firstParkingAvailable);
                holder.tvParkingPrice = (TextView) convertView
                        .findViewById(R.id.firstParkingPrice);
                holder.ivParkingImage = (ImageView)convertView.findViewById(R.id.firstParkingImageView);
                holder.vParkingStatus=null;
                convertView.setTag(holder);

                   break;
               case 1:
                convertView = inflater.inflate(R.layout.park_list_item, null);

                //convertViewCounter++;
                //Log.v(TAG, convertViewCounter + " convertViews have been created");

                holder.tvParkingName = (TextView) convertView
                        .findViewById(R.id.listParkingName);
                holder.tvParkingAvailability = (TextView) convertView
                        .findViewById(R.id.listParkingAvailable);
                holder.tvParkingPrice = (TextView) convertView
                        .findViewById(R.id.listParkingPrice);
                holder.ivParkingImage = (ImageView)convertView.findViewById(R.id.listParkingImageView);

                holder.vParkingStatus = (View)convertView.findViewById(R.id.listParkingStatus);

                convertView.setTag(holder);
                   break;
            }


        } else
            holder = (ViewHolder) convertView.getTag();

        if(parkingList.get(position).getParseFile("image")!=null){
            ParseFile parseImage = parkingList.get(position).getParseFile("image");
            //holder.ivParkingImage.setTag(parseImage.getUrl());

            Ion.with(holder.ivParkingImage)
                .placeholder(R.drawable.estacionat_placeholder)
                .error(R.drawable.estacionat_placeholder)
                .load(parseImage.getUrl());
        }else{
            holder.ivParkingImage.setImageResource(R.drawable.estacionat_placeholder);
        }
        //ParseObject d = (ParseObject) parkingList.get(position);

        if(parkingList.get(position).has(ParseClassesNames.ALIAS))
            holder.tvParkingName.setText(parkingList.get(position).get(ParseClassesNames.ALIAS).toString());
        if(parkingList.get(position).has(ParseClassesNames.PRICE))
            holder.tvParkingPrice.setText("Precio por Hora: "+parkingList.get(position).get("price").toString());
        if(parkingList.get(position).has(ParseClassesNames.CAPACITY))// && parkingList.get(position).has(ParseClassesNames.AVAILABLE))
            holder.tvParkingAvailability.setText("Aforo: " + parkingList.get(position).getInt(ParseClassesNames.CAPACITY)+" puestos");
            //holder.tvParkingAvailability.setText("Disponibles: " + String.valueOf(parkingList.get(position).getInt(ParseClassesNames.AVAILABLE)) + "/" + parkingList.get(position).getInt(ParseClassesNames.CAPACITY));

        if(type==1){
            Resources res = this.c.getResources();
            if(parkingList.get(position).getInt("capacity")>500) {
                int color = res.getColor(R.color.listParkingAvailable);
                GradientDrawable bgShape = (GradientDrawable)holder.vParkingStatus.getBackground();
                bgShape.setColor(color);
            }else if(parkingList.get(position).getInt("capacity")>200){
                int color = res.getColor(R.color.listParkingWarning);
                GradientDrawable bgShape = (GradientDrawable)holder.vParkingStatus.getBackground();
                bgShape.setColor(color);
            }else{
                int color = res.getColor(R.color.listParkingFull);
                GradientDrawable bgShape = (GradientDrawable)holder.vParkingStatus.getBackground();
                bgShape.setColor(color);
            }
        }

        return convertView;

    }
}
