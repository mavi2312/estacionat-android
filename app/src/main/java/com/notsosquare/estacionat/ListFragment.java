package com.notsosquare.estacionat;

import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.notsosquare.estacionat.adapters.ParkingListAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.notsosquare.estacionat.helpers.OrderParkingLots;

import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Roberto on 6/21/2015.
 */
public class ListFragment  extends Fragment {

    private String title;
    private int page;

    private ListView parkingListView;
    private ParkingListAdapter parkingListAdapter;
    private ArrayList<ParseObject> parkingList;

    private RelativeLayout filterBackground;

    OrderParkingLots orderHelper;

    private Location userLocation;

    FloatingActionButton filtersMiniLocation;
    FloatingActionButton filtersMiniFavorites;
    FloatingActionButton filtersMiniPrice;
    FloatingActionButton filtersMiniAvailability;

    public ListFragment() {
        orderHelper = new OrderParkingLots();
    }

    // newInstance constructor for creating fragment with arguments
    public static ListFragment newInstance(int page, String title) {
        ListFragment listFragment = new ListFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        listFragment.setArguments(args);
        return listFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
        orderHelper = new OrderParkingLots();
    }

    //     Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        parkingListView = (ListView)view.findViewById(R.id.parkingList);

        //MainActivity activity = (MainActivity) getActivity();
        //setParkingList(activity.getParkingData());

        FloatingActionButton filters = (FloatingActionButton) view.findViewById(R.id.fab);
        filterBackground = (RelativeLayout)view.findViewById(R.id.fondoFiltros);

        filters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filterBackground.getVisibility()==View.GONE)
                    filterBackground.setVisibility(View.VISIBLE);
                else
                    filterBackground.setVisibility(View.GONE);
            }
        });

        Log.e("Lista","Entro a onCreateView de la lista");

        filtersMiniLocation = (FloatingActionButton) view.findViewById(R.id.fabMini1);

        filtersMiniLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filtersMiniLocation.setBackgroundTintList(getResources().getColorStateList(R.color.selected_filter));
                filtersMiniFavorites.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniPrice.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniAvailability.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                parkingList = orderHelper.orderListLocation(parkingList,userLocation);
                parkingListAdapter.notifyDataSetChanged();
                filterBackground.setVisibility(View.GONE);
            }
        });

        filtersMiniFavorites = (FloatingActionButton) view.findViewById(R.id.fabMini2);
        filtersMiniFavorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filtersMiniFavorites.setBackgroundTintList(getResources().getColorStateList(R.color.selected_filter));
                filtersMiniLocation.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniPrice.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniAvailability.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filterBackground.setVisibility(View.GONE);
            }
        });

        filtersMiniPrice = (FloatingActionButton) view.findViewById(R.id.fabMini3);
        filtersMiniPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filtersMiniPrice.setBackgroundTintList(getResources().getColorStateList(R.color.selected_filter));
                filtersMiniFavorites.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniLocation.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniAvailability.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                parkingList = orderHelper.orderListPrice(parkingList);
                parkingListAdapter.notifyDataSetChanged();
                filterBackground.setVisibility(View.GONE);
            }
        });

        filtersMiniAvailability = (FloatingActionButton) view.findViewById(R.id.fabMini4);
        filtersMiniAvailability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filtersMiniAvailability.setBackgroundTintList(getResources().getColorStateList(R.color.selected_filter));
                filtersMiniFavorites.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniPrice.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));
                filtersMiniLocation.setBackgroundTintList(getResources().getColorStateList(R.color.unselected_filter));

                parkingList = orderHelper.orderListAvailability(parkingList);
                parkingListAdapter.notifyDataSetChanged();
                filterBackground.setVisibility(View.GONE);

            }
        });

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {

            final float scale = getActivity().getResources().getDisplayMetrics().density;
            int pixels = (int) (10 * scale + 0.5f);
            int pixelsRight = (int) (23 * scale + 0.5f);

            RelativeLayout.LayoutParams p1 = (RelativeLayout.LayoutParams)filtersMiniLocation.getLayoutParams();
            p1.setMargins(0,0,pixelsRight,pixels);
            filtersMiniLocation.setLayoutParams(p1);
            RelativeLayout.LayoutParams p2 = (RelativeLayout.LayoutParams)filtersMiniFavorites.getLayoutParams();
            p2.setMargins(0,0,pixelsRight,pixels);
            filtersMiniFavorites.setLayoutParams(p2);
            RelativeLayout.LayoutParams p3 = (RelativeLayout.LayoutParams)filtersMiniPrice.getLayoutParams();
            p3.setMargins(0,0,pixelsRight,pixels);
            filtersMiniPrice.setLayoutParams(p3);
            RelativeLayout.LayoutParams p4 = (RelativeLayout.LayoutParams)filtersMiniAvailability.getLayoutParams();
            p4.setMargins(0,0,pixelsRight,pixels);
            filtersMiniAvailability.setLayoutParams(p4);
        }

        return view;
    }

    public void setParkingList(List<ParseObject> newList){
        ArrayList<ParseObject> temp = new ArrayList<>();

        for(int i = 0;i<newList.size();i++){
            temp.add(newList.get(i));
        }

        parkingList = orderHelper.orderListAvailability(temp);
        parkingListAdapter = new ParkingListAdapter(getActivity(),parkingList);
        parkingListView.setDivider(getResources().getDrawable(R.drawable.borde_lista));
        parkingListView.setAdapter(parkingListAdapter);
    }

    public void setUserLocation(Location googleUserLocation){
        userLocation = googleUserLocation;
    }


}
