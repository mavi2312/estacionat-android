package com.notsosquare.estacionat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.List;

/**
 * Splash screen
 */
public class SplashActivity extends Activity {

    public int objectsCounter;
    public int sizeList;
    public ProgressBar bar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Thread timerThread = new Thread(){

            public void run(){
                try{
                        sleep(3000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }finally{
                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                    }
            }
        };
        timerThread.start();
        bar = (ProgressBar) findViewById(R.id.progressBar);
        bar.setIndeterminate(true);
        //Color color = new Color(getResources().getColor(R.color.naranjaChacao));
        bar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.naranjaChacao), PorterDuff.Mode.SRC_ATOP);


    }

    @Override
    protected void onPause() {
        // Auto-generated method stub
        super.onPause();
        finish();
    }

}

