package com.notsosquare.estacionat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Roberto on 6/21/2015.
 */
public class ParkFragment extends Fragment {

    private String title;
    private int page;

    public ParkFragment() {

    }

    // newInstance constructor for creating fragment with arguments
    public static ParkFragment newInstance(int page, String title) {
        ParkFragment parkFragment = new ParkFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        parkFragment.setArguments(args);
        return parkFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");
    }

    //     Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.park_fragment, container, false);
        return view;
    }
}
