package com.notsosquare.estacionat;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

/**
 * Created by Roberto on 6/21/2015.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                return MapFragment.newInstance(0,"MAPA");
            case 1:
                // Games fragment activity
                return ListFragment.newInstance(1,"LISTA");
            /*case 2:
                // Movies fragment activity
                return ParkFragment.newInstance(2, "ESTACIONAR");*/
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return NUM_ITEMS;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "MAPA";
            case 1:
                return "LISTA";
            /*case 2:
                return "ESTACIONAR";*/
            default:
                return "Page " + position;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }


}
